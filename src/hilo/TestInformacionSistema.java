/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hilo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 *
 * @author docente
 */
public class TestInformacionSistema {
    
    public static void main(String nada[]) throws FileNotFoundException, IOException {
        SysCall mySyscall=new SysCall();
       
        //Cambiar la salida standar a un archivo:
        FileOutputStream salida=new FileOutputStream("src/datos/informacionSistema.txt");
        File fileName=new File("src/datos/informacionSistema.txt");
        //Cambia la salida standar del sistema:
        System.setOut(new PrintStream(salida));
        
     
        String comando1[]={"cmd.exe","/c","date","/T"};
        mySyscall.run(comando1);
        System.out.println("La fecha es: " + mySyscall.getOut());
        
        String comando2[]={"cmd.exe","/c","ipconfig"};
        mySyscall.run(comando2);
        System.out.println("La informacion de red es:\n " + mySyscall.getOut());
         
        String comando3[]={"cmd.exe","/c","notepad.exe","src/datos/informacionSistema.txt"};
        mySyscall.run(comando3);
        
        
        PrintStream inicial=System.out; //almacenan la salida vieja
        System.setOut(inicial);//para volver a imprimir sobre consola
        
        /*System.out.println("holaAA");
        System.out.println("hola2");*/
   
        
        salida.close();
        
        
        
        
             
        
        
    }
    
}
